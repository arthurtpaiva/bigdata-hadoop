## Compilando

```
javac -classpath hadoop-core-1.2.1.jar -d bin *.java
jar -cvf hadoop-job.jar -C bin/ .
```

## Executando WordCount

```
hadoop fs -mkdir input_data
hadoop fs -put input.data input_data
hadoop jar hadoop-job.jar WordCount input_data output_data
```

## Executando Bolsa Familia

```
hadoop jar hadoop-job.jar BolsaFamilia bolsa-familia-sample output_bolsa
hadoop jar hadoop-job.jar BolsaFamilia bolsa-familia output_bolsa
```

## Checando o resultado

```
hadoop fs -ls output_data
hadoop fs -cat output_data/part-00000
```